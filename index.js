const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const mongoose = require("mongoose");
const cors = require("cors");
const requireDir = require("require-dir");

requireDir("./src/models");
mongoose.connect("mongodb+srv://usuario:senha@banco-j4cwf.mongodb.net/test?retryWrites=true&w=majority", { useNewUrlParser: true });

const app = express();
const server = require("http").Server(app);

app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/api", require("./src/routes"));

server.listen(3333);
