const express = require("express");
const routes = express.Router();

require("dotenv").config({ path: __dirname + "/.env" });
var jwt = require("jsonwebtoken");

function verifyJWT(req, res, next) {
  var token = req.headers["x-access-token"];
  if (!token) return res.status(401).send({ auth: false, message: "No token provided." });

  jwt.verify(token, process.env.SECRET, function(err, decoded) {
    if (err) return res.status(500).send({ auth: false, message: "Failed to authenticate token." });

    // se tudo estiver ok, salva no request para uso posterior
    req.userId = decoded.id;
    next();
  });
}

routes.get("/", (req, res) => {
  return res.send("Backend rodando na porta 3333");
});

routes.get("/logout", function(req, res) {
  return res.status(200).send({ auth: false, token: null });
});

const UsuarioController = require("./controllers/UsuariosController");
routes.get("/usuarios", verifyJWT, UsuarioController.getAll);
routes.post("/usuarios", verifyJWT, UsuarioController.incluir);
routes.get("/usuarios/:id", verifyJWT, UsuarioController.getByID);
routes.put("/usuarios/:id", verifyJWT, UsuarioController.editar);
routes.delete("/usuarios/:id", verifyJWT, UsuarioController.excluir);

const AdminController = require("./controllers/AdminController");
routes.get("/admin", AdminController.getAll);
routes.post("/login", AdminController.login);

module.exports = routes;
