const Admin = require("../models/Admin");

require("dotenv").config();
var jwt = require("jsonwebtoken");

module.exports = {
  async login(req, res) {
    const registro = await Admin.findOne({ login: req.body.login, senha: req.body.senha });

    if (registro) {
      var token = jwt.sign({ id: registro._id }, process.env.SECRET, {
        expiresIn: 60 * 60 * 5
      });

      return res.status(200).send({ auth: true, token: token });
    }

    return res.status(500).send("Login inválido!");
  },

  async getAll(req, res) {
    const registros = await Admin.find({});
    return res.json(registros);
  }
};
