const Usuario = require("../models/Usuario");

module.exports = {
  async getAll(req, res) {
    const registros = await Usuario.find({});
    return res.json(registros);
  },

  async getByID(req, res) {
    const registro = await Usuario.findById(req.params.id);
    return res.json(registro);
  },

  async incluir(req, res) {
    const registro = await Usuario.create(req.body);
    return res.json(registro);
  },

  async editar(req, res) {
    const registro = await Usuario.findByIdAndUpdate(req.params.id, req.body, { new: true });
    return res.json(registro);
  },

  async excluir(req, res) {
    await Usuario.findByIdAndRemove(req.params.id);
    return res.send();
  }
};
