const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema({
    login: String,
    senha: String
}, { timestamps: true })

module.exports = mongoose.model('Admin', AdminSchema)